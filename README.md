# util

Different utilities written to solve various problems but do not warrant a separate project.

### cFormat

Allows to format c/cpp code nicely. Works well on code stored on a single line or
with partial space formatting. 

> format filename > output