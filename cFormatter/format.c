/*
    Copyright (C) 2019  Vlad C

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
/*
    useful for formatting cpp code when it is stored as a single line with no tabs
    or without any spaces at all (ie copied from pdf).
    can work with partially formatted code but not tabs.
*/
#include <stdio.h> 

#define TAB 4

int fpeek(FILE *stream)
{
    int c;

    c = fgetc(stream);
    ungetc(c, stream);

    return c;
}

void indent(int brackets, FILE *fp){
    for(int i=0;i<brackets;i++){ //# of tabs
        for(int j=0;j<TAB;j++){ //# of spaces per tab
            int nc = fpeek(fp);
            if(nc==' '){ //if next is space print it and eat it
                char c = fgetc(fp);
                printf("%c",c);
            }else{
                printf(" ");
            }
        }
    }

}

int main (int argc, char **argv) 
{ 
    if (argc != 2){
        printf("Usage: format filename\n");
        return 0;
    }

    // open the file 
    FILE *fp = fopen(argv[1],"r"); 
    
    int state=0;
    int brackets=0;
    
    // Return if could not open file 
    if (fp == NULL) 
      return 0; 
  
    do
    { 
        // Taking input single character at a time 
        char c = fgetc(fp); 
  
        // Checking for end of file 
        if (feof(fp)) 
            break ; 

        switch(c){
            case '#':
                state = 1; //seen #
                printf("\n%c", c); 
                indent(brackets, fp);
            break;
            case ';':
                printf("%c\n", c); 
                indent(brackets, fp);
            break;
            case '>':
                if(state==1){
                    printf("%c\n",c);
                    state = 0;
                }else{
                    printf("%c",c);
                }
            break;
            case '{':
                brackets++;
                printf("%c\n",c);
                indent(brackets, fp);
            break;
            case '}':
                if(brackets > 0) brackets--;
                printf("\n");
                indent(brackets, fp);
                printf("%c\n",c);
                indent(brackets, fp);
            break;
            case '\n':
                printf("%c",c);
                indent(brackets, fp);
            break;
            default:
                printf("%c", c);     
        }
        
    }  while(1); 
  
    fclose(fp); 
    return(0); 
} 